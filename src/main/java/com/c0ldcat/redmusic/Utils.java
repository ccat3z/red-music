package com.c0ldcat.redmusic;

import java.util.Date;

public class Utils {

    public static void log(String log){
        log(log, true);
    }

    public static void log(String log, boolean b){
        System.out.println( (b ? "[" + new Date() + "]" : "") + log);
    }
}
