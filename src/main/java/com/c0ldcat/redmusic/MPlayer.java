package com.c0ldcat.redmusic;

import com.c0ldcat.netease.music.Playlist;
import com.c0ldcat.netease.music.Song;

import java.io.PrintStream;

public class MPlayer {
    private Process mplayerProcess;
    private PrintStream in;
    private LineProcess lineProcess;

    private Playlist currentPlayList;
    private int playListIndex;
    private int mode = MODE_LOOP;

    private Song currentSong;

    static final int MODE_LOOP = 0;
    static final int MODE_SINGLE = 1;
    static final int MODE_RANDOM = 2;

    MPlayer() {
        try {
            mplayerProcess = Runtime.getRuntime().exec("mplayer -novideo -vo null -vc null -slave -quiet -idle -msglevel all=6");
        } catch (Exception e){
            e.printStackTrace();
        }
        in = new PrintStream(mplayerProcess.getOutputStream());
        lineProcess = new LineProcess(mplayerProcess.getInputStream());
        lineProcess.start();
        lineProcess.registerListener("EOF code: 1", new LineProcess.MessageListener() {
            @Override
            public void handle(String msg) {
                onEof();
            }
        });
    }

    private void command(String cmd) {
        Utils.log("MPlayer run:" + cmd);
        in.println(cmd);
        in.flush();
    }

    private void play(Song song) {
        command("loadfile " + "\"" + song.getCache() + "\"");
    }

    boolean play(Playlist pl) {
        return play(pl, 0);
    }

    private void play(int index){
        if (index >= 0 & index < currentPlayList.size()){
            playListIndex = index;
            currentSong = currentPlayList.get(index);
            play(currentSong);
        } else if (index < 0){
            play(index + currentPlayList.size());
        } else {
            play(index - currentPlayList.size());
        }
    }

    boolean play(Playlist pl, int index){
        if (pl == null) return false;
        if (pl.size() == 0) return false;
        currentPlayList = pl;
        play(index);
        return true;
    }

    void setMode(int m){
        if(m == MODE_LOOP | m == MODE_SINGLE | m == MODE_RANDOM){
            mode = m;
        }
    }

    void playNext(){
        switch (mode){
            case MODE_LOOP:
                play(++playListIndex);
                break;
            case MODE_SINGLE:
                play(playListIndex);
                break;
            case MODE_RANDOM:
                play((int) (Math.random() * currentPlayList.size()));
                break;
        }
    }

    void playNext(boolean force){
        if (force) play(++playListIndex);
    }

    void playPre(){
        switch (mode){
            case MODE_LOOP:
                play(--playListIndex);
                break;
            case MODE_SINGLE:
                play(playListIndex);
                break;
            case MODE_RANDOM:
                play((int) (Math.random() * currentPlayList.size()));
                break;
        }
    }

    void pause() {
        command("pause");
    }

    void volumeUp(){
        command("volume +1");
    }

    void volumeDown(){
        command("volume -1");
    }

    void playPre(boolean force){
        if (force) play(--playListIndex);
    }

    void quit() {
        command("quit");
    }

    void waitFor() {
        try {
            mplayerProcess.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void onEof() {
        playNext();
    }

    Playlist getCurrentPlayList() {
        return currentPlayList;
    }

    Song getCurrentSong() {
        return currentSong;
    }
}
