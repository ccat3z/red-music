package com.c0ldcat.redmusic;

import fi.iki.elonen.NanoHTTPD;

public interface Task {
    public NanoHTTPD.Response handle(TaskHelper th);
}
