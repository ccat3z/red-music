package com.c0ldcat.redmusic;

import com.c0ldcat.netease.music.*;
import fi.iki.elonen.NanoHTTPD;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RedMusic extends NanoHTTPD{
    private NetEaseMusic netEaseMusic;
    private HashMap<String, Task> actionMap;
    private MPlayer mPlayer;

    final static private String ACTION_GET_PLAYLISTS = "get-playlists";
    final static private String ACTION_GET_PLAYLIST = "get-playlist";
    final static private String ACTION_UPDATE_PLAYLISTS = "update-playlists";
    final static private String ACTION_UPDATE_PLAYLIST = "update-playlist";
    final static private String ACTION_CACHE_PLAYLIST = "cache-playlist";
    final static private String ACTION_PLAY_PLAYLIST = "play-playlist";
    final static private String ACTION_PLAY_SONG = "play-song";
    final static private String ACTION_GET_PLAYING_PLAYLIST = "get-playing-playlist";
    final static private String ACTION_GET_PLAYING_SONG = "get-playing-song";
    final static private String ACTION_VOLUME_UP = "volume-up";
    final static private String ACTION_VOLUME_DOWN = "volume-down";
    final static private String ACTION_PAUSE = "pause";
    final static private String ACTION_PLAY_NEXT = "next";
    final static private String ACTION_PLAY_PRE = "pre";
    final static private String ACTION_SET_MODE = "set-mode";


    public static void main(String[] args) throws Exception{
        //read netease music user info
        String username = System.getenv("NM_USER");
        String password = System.getenv("NM_PASS");
        String configFile = System.getenv("NM_CONFIG");
        String cacheDir = System.getenv("NM_CACHE");

        if ( username == null || password == null || configFile == null || cacheDir == null ) {
            Utils.log("netease music launch failed");
            return;
        }

        RedMusic s;
        if (args.length != 2) {
            s = new RedMusic("0.0.0.0", 1234, username, password, configFile, cacheDir);
        } else {
            s = new RedMusic(args[0],Integer.parseInt(args[1]), username, password, configFile, cacheDir);
        }

        s.start();
        while (true);
    }

    public RedMusic(String host, int port, String username, String password, String configFile, String cacheDir) {
        //create server
        super(host,port);

        mPlayer = new MPlayer();

        actionMap = new HashMap<>();

        actionMap.put(ACTION_GET_PLAYLISTS, new GetPlaylistsHandler());
        actionMap.put(ACTION_GET_PLAYLIST, new GetPlaylistHandler());
        actionMap.put(ACTION_UPDATE_PLAYLISTS, new UpdatePlaylistsHandler());
        actionMap.put(ACTION_UPDATE_PLAYLIST, new UpdatePlaylistHandler());
        actionMap.put(ACTION_CACHE_PLAYLIST, new CachePlaylistHandler());
        actionMap.put(ACTION_PLAY_PLAYLIST, new PlayPlayListHandler());
        actionMap.put(ACTION_PLAY_SONG, new PlaySongHandler());
        actionMap.put(ACTION_GET_PLAYING_PLAYLIST, new GetPlayingPlaylistHandler());
        actionMap.put(ACTION_GET_PLAYING_SONG, new GetPlayingSongHandler());
        actionMap.put(ACTION_VOLUME_UP, new VolumeUpHandler());
        actionMap.put(ACTION_VOLUME_DOWN, new VolumeDownHandler());
        actionMap.put(ACTION_PAUSE, new PauseHandler());
        actionMap.put(ACTION_PLAY_NEXT, new PlayNextHandler());
        actionMap.put(ACTION_PLAY_PRE, new PlayPreHandler());
        actionMap.put(ACTION_SET_MODE, new SetModeHandler());

        netEaseMusic = new NetEaseMusic(configFile);
        if(! netEaseMusic.isLogin()) {
            Utils.log("try to login...");
            netEaseMusic.login(username, password);
        }
        netEaseMusic.setCacheDir(cacheDir);

        Utils.log("Server will start on " + host + ":" + port + ".", false);
    }

    @Override
    public Response serve(IHTTPSession session) {
        TaskHelper th = new TaskHelper(session);

        ArrayList<String> path = th.getPath();
        if (path.size() != 0) {
            String action = path.get(0);
            if (actionMap.containsKey(action)) {
                return actionMap.get(action).handle(th);
            }
        }

        //return index page
        return newChunkedResponse(Response.Status.OK, "text/html", getClass().getResourceAsStream("/PlaylistList.html"));
    }

    private class IdHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            Map<String, String> pars = th.getParameters();
            if (pars.containsKey("id")){
                try {
                    int id = Integer.parseInt(pars.get("id"));
                    return handleId(id);
                } catch (NumberFormatException e) {
                    return NanoHTTPD.newFixedLengthResponse("wrong parameter");
                }
            } else {
                return NanoHTTPD.newFixedLengthResponse("no parameter");
            }
        }

        public Response handleId(int id){
            return newFixedLengthResponse("done");
        }
    }

    private class GetPlaylistsHandler implements Task {
        final static String PLAYLISTS = "playlists";
        final static String NAME = "name";
        final static String ID = "id";

        @Override
        public Response handle(TaskHelper th) {
            JSONObject r = new JSONObject();
            JSONArray plsJson = new JSONArray();
            for (Playlist p : netEaseMusic.getPlaylists()) {
                JSONObject plJson = new JSONObject();
                plJson.put(NAME, p.getName());
                plJson.put(ID, p.getId());
                plsJson.put(plJson);
            }
            r.put(PLAYLISTS, plsJson);

            return newFixedLengthResponse(r.toString());
        }
    }

    private class GetPlaylistHandler extends IdHandler {
        final static String NAME = "name";
        final static String ID = "id";
        final static String SONG_LIST = "song_list";
        final static String SONG_NAME = "song_name";
        final static String SONG_ID = "song_id";

        @Override
        public Response handleId(int id) {
            try {
                Playlist pl = netEaseMusic.getPlaylist(id);

                JSONObject r = new JSONObject();
                r.put(NAME, pl.getName());
                r.put(ID, pl.getId());

                JSONArray songListJson = new JSONArray();
                for (Song s : pl) {
                    JSONObject songJson = new JSONObject();
                    songJson.put(SONG_NAME, s.getName());
                    songJson.put(SONG_ID, s.getId());
                    songListJson.put(songJson);
                }
                r.put(SONG_LIST, songListJson);

                return newFixedLengthResponse(r.toString());
            } catch (ConfigNoFoundException e) {
                return newFixedLengthResponse("{}");
            }
        }
    }

    private class UpdatePlaylistsHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            try {
                netEaseMusic.updateUserPlaylist();
                return NanoHTTPD.newFixedLengthResponse("done");
            } catch (NoLoginException e) {
                return NanoHTTPD.newFixedLengthResponse("no login");
            }
        }
    }

    private class UpdatePlaylistHandler extends IdHandler {
        @Override
        public Response handleId(int id) {
            try {
                Playlist pl = netEaseMusic.getPlaylist(id);
                pl.update();
                return newFixedLengthResponse("done");
            } catch (ConfigNoFoundException e) {
                return NanoHTTPD.newFixedLengthResponse("error");
            }
        }
    }

    private class CachePlaylistHandler extends IdHandler {
        @Override
        public Response handleId(int id) {
            try {
                final Playlist pl = netEaseMusic.getPlaylist(id);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            pl.cacheAll();
                        } catch (NoLoginException e) {
                            //ignore
                        }
                    }
                }).start();

                return newFixedLengthResponse("done");
            } catch (ConfigNoFoundException e) {
                return NanoHTTPD.newFixedLengthResponse("error");
            }
        }
    }

    private class PlayPlayListHandler extends IdHandler {
        @Override
        public Response handleId(int id) {
            try {
                Playlist pl = netEaseMusic.getPlaylist(id);

                mPlayer.play(pl);

                return newFixedLengthResponse("done");
            } catch (ConfigNoFoundException e) {
                return NanoHTTPD.newFixedLengthResponse("error");
            }
        }
    }

    private class PlaySongHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            Map<String, String> pars = th.getParameters();
            if (pars.containsKey("id") && pars.containsKey("pid")){
                try {
                    int id = Integer.parseInt(pars.get("id"));
                    int pid = Integer.parseInt(pars.get("pid"));
                    try {
                        Playlist pl = netEaseMusic.getPlaylist(pid);

                        int i;
                        for (i = 0 ; i < pl.size() ; i++ ) {
                            if ( pl.get(i).getId() == id )
                                break;
                        }

                        mPlayer.play(pl, i);

                        return newFixedLengthResponse("done");
                    } catch (ConfigNoFoundException e) {
                        return NanoHTTPD.newFixedLengthResponse("error");
                    }
                } catch (NumberFormatException e) {
                    return NanoHTTPD.newFixedLengthResponse("wrong parameter");
                }
            } else {
                return NanoHTTPD.newFixedLengthResponse("no parameter");
            }
        }
    }

    private class GetPlayingPlaylistHandler implements Task {
        private final static String NAME = "name";
        private final static String ID = "id";

        @Override
        public Response handle(TaskHelper th) {
            Playlist pl = mPlayer.getCurrentPlayList();

            JSONObject r = new JSONObject();
            if (pl != null) {
                r.put(NAME, pl.getName());
                r.put(ID, pl.getId());
            }

            return newFixedLengthResponse(r.toString());
        }
    }

    private class GetPlayingSongHandler implements Task {
        private final static String NAME = "name";
        private final static String ID = "id";

        @Override
        public Response handle(TaskHelper th) {
            Song s = mPlayer.getCurrentSong();

            JSONObject r = new JSONObject();
            if (s != null) {
                r.put(NAME, s.getName());
                r.put(ID, s.getId());
            }

            return newFixedLengthResponse(r.toString());
        }
    }

    private class VolumeUpHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            mPlayer.volumeUp();
            return newFixedLengthResponse("done");
        }
    }

    private class VolumeDownHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            mPlayer.volumeDown();
            return newFixedLengthResponse("done");
        }
    }

    private class PauseHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            mPlayer.pause();
            return newFixedLengthResponse("done");
        }
    }


    private class PlayNextHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            mPlayer.playNext();
            return newFixedLengthResponse("done");
        }
    }

    private class PlayPreHandler implements Task {
        @Override
        public Response handle(TaskHelper th) {
            mPlayer.playPre();
            return newFixedLengthResponse("done");
        }
    }

    private class SetModeHandler extends IdHandler {
        @Override
        public Response handleId(int id) {
            switch (id) {
                case MPlayer.MODE_LOOP:
                case MPlayer.MODE_SINGLE:
                case MPlayer.MODE_RANDOM:
                    mPlayer.setMode(id);
                    return newFixedLengthResponse("done");
                default:
                    return newFixedLengthResponse("bad mode");
            }
        }
    }
}
